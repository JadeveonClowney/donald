import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadDataService {

  constructor(private http: HttpClient) {}
   getmeme(): Observable<any>{
     return  this.http.get('https://www.tronalddump.io/random/quote').pipe
   (
     map(resp=>resp)
   );}
  /*getmeme(){
    return this.http.get('https://www.tronalddump.io/random/quote');
  }
  */
}
