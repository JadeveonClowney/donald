import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotusComponent } from './potus.component';

describe('PotusComponent', () => {
  let component: PotusComponent;
  let fixture: ComponentFixture<PotusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
