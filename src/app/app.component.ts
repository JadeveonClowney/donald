import { Component } from '@angular/core';
import { LoadDataService } from './load-data.service'
import {first} from 'rxjs/operators'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Donald';
  meme;
  constructor(private loaddata:LoadDataService){}
  ngOnInit(){this.loaddata.getmeme().subscribe(resp => {
    this.meme = resp['value'];
    console.log(this.meme);
  })}
}
